<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Asset;
use App\Category;
use App\User;
use App\Status;

class Transaction extends Model
{
    public function category(){
        return $this->belongsTo('\App\Category');
    }

    public function asset(){
        return $this->belongsTo('\App\Asset');
    }

    public function user(){
        return $this->belongsTo('\App\User');
    }

    public function status(){
        return $this->belongsTo('\App\Status');
    }
}
