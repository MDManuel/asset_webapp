<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Asset;
use App\Transaction;

class Category extends Model
{
    public function assets(){
        return $this->hasMany('\App\Asset');
    }

    public function transactions(){
        return $this->hasMany('\App\Transaction');
    }
}
