<?php

namespace App\Http\Controllers;

use App\Asset;
use Carbon\Carbon;
use App\Category;
use Illuminate\Http\Request;

class AssetController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $assets = Asset::all();
        //$categories = Category::all();

        return view('assets.index')->with('title', 'Assets')->with('assets', $assets);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $assets = Asset::all();
        $categories = Category::where('isActive',1)->get();

        return view('assets.create')->with('title', 'Assets')->with('assets', $assets)->with('categories', $categories);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $rules = array(
            "name" => "required",
            "description" => "required",
            "image" => "image|mimes:jpeg,png,jpg,gif,svg|max:2048",
            "category" => "required"
        );

        $this->validate($request, $rules);

        $asset = new Asset();
        $asset->name = $request->input('name');
        $asset->serialNo = "";
        $asset->description = $request->input('description');
        $asset->category_id = $request->input('category');
        
        $asset->isActive = true;

        if ($request->file('image') == "" || $request->file('image')== null){
            $asset->image_path = asset('/images/no_image.jpg');
        } else {
            //handle image file upload
            $image = $request->file('image');
            //set the file name
            $file_name = time().".". $image->getClientOriginalExtension();
            $destination = "images/";
            $image->move($destination, $file_name);

            $asset->image_path = $destination.$file_name;
        }

        if($asset->save()){
            $id = $asset->id;
            $asset = Asset::find($id);
            $asset->serialNo = Carbon::now()->format('md') . "_" . $asset->category->category_name . "_" . $id;
            $asset->save();

            $request->session()->flash('status', 'Asset with serial number: ' . $asset->serialNo . ' successfully added!');
            return redirect("/assets");
        }else{
            $request->session()->flash('status', 'Asset creation unsuccessful');
            return redirect("/assets");
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {   

        //dd('here @ show');

        $asset = Asset::findOrFail($id);
        $categories = Category::all();
        $image_path = $asset->image_path;
        
        return view('assets.show')->with('asset', $asset)->with('categories', $categories)->with('image', $image_path);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $asset = Asset::find($id);
        $categories = Category::all();
        return view('assets.edit')->with('asset', $asset)->with('categories', $categories)->with('title', 'Edit Assets');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        
        $asset = Asset::find($id);
        $rules = array(
            "name" => "required",
            "description" => "required",
            "image" => "image|mimes:jpeg,png,jpg,gif,svg|max:2048",
            "category" => "required"
        );

        $this->validate($request, $rules);

        $asset->name = $request->input('name');
        $asset->description = $request->input('description');
        $asset->category_id = $request->input('category');
        $asset->isActive = true;

        if ($request->file('image') != "" || $request->file('image') != null){
            
                $image = $request->file('image');
                //set the file name
                //$file_name_without_ext = $image->getClientOriginalName();
                $file_name = time()."_".$asset->category_id.".".$image->getClientOriginalExtension();
                //$file_name = time().".".$image->getClientOriginalExtension();
                $destination = "images/";
                $image->move($destination, $file_name);

                $asset->image_path = $destination.$file_name;
            
        } else {
            $image = 'no_image.jpg';
            $destination = "images/";
            $asset->image_path = $destination.$image;
        }

        if($asset->save()){
            $request->session()->flash('status', 'Asset successfully updated!');
            return redirect("/assets");
        }else{
            $request->session()->flash('status', 'Asset not updated.');
            return redirect("/assets");
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Asset  $asset
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $asset = Asset::find($id);

        if($asset->isActive == true){
            $asset->isActive = false;
        }else{
            $asset->isActive = true;
        }

        if($asset->save()){
            if($asset->isActive == true){
                return redirect('/assets')->with('status', "$asset->name asset is activated")->with('title', 'Assets');
            }
            else{
                return redirect('/assets')->with('status', "$asset->name asset is deactivated")->with('title', 'Assets');
            }
        };
    }

    
}
