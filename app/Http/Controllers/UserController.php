<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Users";

        $users = User::all();
        return view('users.index')->with('users', $users)->with('title', $title);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $user = User::find($id);
        return view('users.edit')->with('user', $user);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $title = "Status";
        $user = User::find($id);

        $user->name = $request->input('name');
        $user->email = $request->input('email');

        //return view('statuses.edit')->with('res', 'been here')->with('status', $status);
        if($user->save()){
            return redirect('/users')->with('status', 'User successfully updated')->with('title', $title);
        };
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $title = "Status";
        $user = User::find($id);

        if($user->isActive == true){
            $user->isActive = false;
        }else{
            $user->isActive = true;
        }

        if($user->save()){
            if($user->isActive == true){
                return redirect('/users')->with('status', "$user->name's profile is activated")->with('title', $title);
            }
            else{
                return redirect('/users')->with('status', "$user->name's profile is deactivated")->with('title', $title);
            }
        };
    }
}
