<?php

namespace App\Http\Controllers;

use App\Status;
use Illuminate\Http\Request;

class StatusController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Status";

        $statuses = Status::all();
        return view('statuses.index')->with('statuses', $statuses)->with('title', $title);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = "Status";
        $name = $request->input('status');

        $status = new Status;
        $status->status = $name;
        $status->isActive = true;
        if($status->save()){
            return redirect('/statuses')->with('status', 'New status successfully added')->with('title', $title);
        };
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $status = Status::find($id);
        return view('statuses.edit')->with('status', $status);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $title = "Status";
        $status = Status::find($id);

        $status->status = $request->input('status');

        //return view('statuses.edit')->with('res', 'been here')->with('status', $status);
        if($status->save()){
            return redirect('/statuses')->with('status', 'Status updated')->with('title', $title);
        };
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Status  $status
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $status = Status::find($id);

        if($status->isActive == true){
            $status->isActive = false;
        }else{
            $status->isActive = true;
        }

        if($status->save()){
            if($status->isActive == true){
                return redirect('/users')->with('status', "$status->name status is activated")->with('title', 'Status');
            }
            else{
                return redirect('/users')->with('status', "$status->name status is deactivated")->with('title', 'Status');
            }
        };
    }
}
