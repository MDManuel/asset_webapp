<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $title = "Categories";

        $categories = Category::all();
        return view('categories.index')->with('categories', $categories)->with('title', $title);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $title = "Category";
        $name = $request->input('category');

        $category = new Category;
        $category->category_name = $name;
        $category->isActive = true;
        if($category->save()){
            return redirect('/categories')->with('status', 'New category successfully added')->with('title', $title);
        };
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $category = Category::find($id);
        return view('categories.edit')->with('category', $category);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        $title = "Categories";
        $category = Category::find($id);

        $category->category_name = $request->input('category');

        //return view('statuses.edit')->with('res', 'been here')->with('status', $status);
        if($category->save()){
            return redirect('/categories')->with('status', 'Category updated')->with('title', $title);
        };
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $title = "Status";
        $category = Category::find($id);

        if($category->isActive == true){
            $category->isActive = false;
        }else{
            $category->isActive = true;
        }
 
        if($category->save()){
            return redirect('/categories')->with('status', 'Status updated')->with('title', $title);
        };
    }
}
