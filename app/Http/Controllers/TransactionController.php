<?php

namespace App\Http\Controllers;

use App\Transaction;
use App\Status;
use App\Asset;
use App\User;
use App\Category;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Enumerable;

class TransactionController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(auth()->user()->role_id === 1 ){
            $transactions = Transaction::orderBy('status_id', 'asc')->get();
            $statuses = Status::all();
            $assets = Asset::all();

            return view('transactions.index')->with('title', 'Transactions')->with('transactions', $transactions)->with('statuses', $statuses)->with('assets', $assets);
        } else {
            $id = Auth()->User()->id;
            $transactions = Transaction::orderBy('status_id','asc')->where('user_id', $id)->get();
            //dd($transactions);
            //$statuses = Status::all();
            $assets = Asset::where('isActive', true)->get();

            return view('transactions.index')->with('title', 'Transactions')->with('transactions', $transactions);
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create($id = null)
    {
        
        if(($id)==null){
            $assets = Asset::Where('isActive', true)->get();
            $categories = Category::Where('isActive', true)->get();

            return view('transactions.create')->with('assets', $assets)->with('categories', $categories)->with('title', 'Our Luxurious Escapades Collection');
        } else {

            //dd($id);

            //return ("http://localhost:8000/transactions/create/" . $id);

            $assets = Asset::orderBy('name', 'desc')->where('category_id', $id)->where('isActive',true)->get();
            $categories = Category::all();

            if(collect($assets)->isNotEmpty()) {
                return view('transactions/create')->with('assets', $assets)->with('categories', $categories)->with('title', 'Car Selection Sorted');
            } else {
                return view('transactions/create')->with('assets', $assets)->with('categories', $categories)->with('status', 'No available Luxurious Escapades for rent at the moment. Please contact us later.');
            }

            

            // return redirect()->to('transactions/create/'+$id)->send();

        }
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $borrowDate = new Carbon($request->input('modal_borrowDate', 'Asia/Taipei'));
        $returnDate = new Carbon($request->input('modal_returnDate', 'Asia/Taipei'));
        $now = new Carbon('now', 'Asia/Taipei');
        $totalDays = $returnDate->diffInDays($borrowDate);
        if($totalDays <= 0 ){
            $totalDays = 1;
        } 
        //$elapsed = $returnedDate->diffForHumans();

        //$formattedreturnDate = $parsedDate->format('m-d-Y');
        //dd($totalDays);

        $rules = array(
            "carPickedModal_Id" => "required",
            "modal_borrowDate" => "required|date",
            "modal_borrowDate" => "required|date",
        );

        $this->validate($request, $rules);


        $asset_id = $request->input('carPickedModal_Id');
        $asset = Asset::find($asset_id);
        $id = Auth::id();
        $user = User::find($id);
        $notesTimestamp = auth::user()->name . " @ " . Carbon::now()->format('m-d-Y H:i:s') . ": ";

        $asset->isActive = false;
        $asset->save();
        //dd($asset);

        $transaction = new Transaction;
        $transaction->image_path = $asset->image_path;
        $transaction->refNo = "";
        $transaction->asset_id = $asset_id;
        $transaction->serialNo = $asset->serialNo;
        $transaction->user_id = $id;
        $transaction->borrowDate = $borrowDate;
        $transaction->returnDate = $returnDate;
        $transaction->returnDate = Carbon::today();
        $transaction->totalDays = $totalDays;
        //$transaction->req_notes = nl2br($notesTimestamp . $request->input('req_notes') . "\n");
        $transaction->req_notes = $request->input('req_notes');
        $transaction->admin_notes = "";

        // for days lapsed inputted on the table for the first time
        

        if($transaction->save()){
            $id = $transaction->id;
            $transaction = Transaction::find($id);
            $transaction->refNo = Carbon::now()->format('md-Y') . "-" . strtoupper(substr($asset->category->category_name, 0, 5)) . "-" . strtoupper(substr($user->name, 0, 3)) . "-" . $id;
            //dd($transaction->refNo);
            $transaction->save();

            return redirect('/transactions')->with('status', 'Transaction complete.')->with('title', 'Jumbotron Car Rental');
        } else {
             return redirect('/transactions')->with('status', 'Warning: Unable to complete the transaction')->with('title', 'Jumbotron Car Rental');
        };
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $transaction = Transaction::find($id);
        //return dd($transaction);
        return response()->json([
            'message' => "Transaction Found",
            'data' => $transaction
        ], 200);
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function edit(Transaction $transaction)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function update($id, Request $request)
    {
        //return dd($request->input('transActionModalBody_Action'));
        $action = $request->input('transActionModalBody_Action');


        $transaction = Transaction::find($id);
        $assetId = $transaction->asset_id;
        $selectedAsset = Asset::find($assetId);
        //dd($selectedAsset);
        $oldNotes = $transaction->admin_notes;
        $reqOldNotes = $transaction->req_notes;
        $notesTimestamp = auth::user()->name . " @ " . Carbon::now()->format('m-d-Y H:i:s') . ": ";
        
        if($action == 'approved'){
            $transaction->status_id = 2;
            $transaction->admin_notes = nl2br($oldNotes . $notesTimestamp . $request->input('textAdmin') . "\n");
            $selectedAsset->isActive = false;
            $selectedAsset->save();
        } else if ($action == 'declined'){   
            $transaction->status_id = 3;
            $selectedAsset->isActive = true;
            $selectedAsset->save();
            $transaction->admin_notes = nl2br($oldNotes . $notesTimestamp . $request->input('textAdmin') . "\n");
        } else if ($action == 'cancelled'){
            //dd($action);
            $transaction->status_id = 4;
            $selectedAsset->isActive = true;
            $selectedAsset->save();
            $transaction->req_notes = nl2br($reqOldNotes . $notesTimestamp . $request->input('textAdmin') . "\n");
        } else if ($action == 'returned'){
            $transaction->status_id = 5;
            $transaction->admin_notes = nl2br("\n\n". $oldNotes . $notesTimestamp . "-- RETURNED --" . "\n");
            $selectedAsset->isActive = true;
            $selectedAsset->save();
            $transaction->clearedDate = Carbon::now();
        }

        if($transaction->save()){
            if($action == 'approved'){
                $request->session()->flash('status', 'Request approved.');
            } else if ($action == 'declined'){
                $request->session()->flash('status', 'Request declined.');
            } else if ($action == 'cancelled'){
                $request->session()->flash('status', 'Request cancelled.');
            } else if ($action == 'returned'){
                $request->session()->flash('status', 'Asset returned.');
            }
            
            return redirect('/transactions');
        }else{
            $request->session()->flash('status', 'Unable to complete the transaction.');
            return redirect('/transactions');
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Transaction  $transaction
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        return dd('test');
    }

    public function assetImageGetter($id)
    {
        
        $asset = Asset::findOrFail($id);
        $category = Category::findOrFail($asset->category_id);
        $category_name = $category->category_name;

        //dd($asset->image_path);

        return response()->json([
            'message' => "Asset found",
            'data' => "
            <div class=\"container\">
                <div class=\"form-group text-center\">
                    <img src=\"http://localhost:8000/$asset->image_path\" style=\"height:250px; width: 100%; object-fit: cover;\">
                </div>
                <div class=\"form-group\">
                    <label for=\"name\">Asset: </label>
                    <input disabled type=\"text\" name=\"name\" id=\"name\" class=\"form-control\" value=\"$asset->name\">
                </div>
                <div class=\"form-group\">
                    <label for=\"serialNo\">Control No.: </label>
                    <input disabled type=\"text\" name=\"serialNo\" id=\"serialNo\" class=\"form-control\" value=\"$asset->serialNo\">
                </div>
                <div class=\"form-group\">
                    <label for=\"description\">Description: </label>
                    <textarea disabled name=\"description\" id=\"description\" class=\"form-control\">$asset->description</textarea>
                </div>
                <div class=\"form-group\">
                    <label for=\"category\">Category: </label>
                    <input type=\"text\" disabled name=\"category\" id=\"category\" class=\"form-control\" value=\"$category_name\">
                </div>
            </div>
            "
        ],201);
    }

    public function categorySelector($id)
    {
        $assets = Asset::orderBy('name', 'desc')->where('category_id', $id)->where('isActive',1)->get();
        $categories = Category::where('isActive',1)->get();

        return view('transactions/create')->with('assets', $assets)->with('categories', $categories)->with('title', 'Car Selection');

    }   
}
