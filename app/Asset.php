<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Category;
use App\Transaction;

class Asset extends Model
{
    public function category(){
        return $this->belongsTo('\App\Category');
    }

    public function transactions(){
        return $this->hasMany('\App\Transactions');
    }
}
