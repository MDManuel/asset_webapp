//console.log("approve JS");

const actionBtn = document.querySelectorAll('.actionBtn');
const txtAdmin = document.querySelector('#txtAdmin');
const proceedActionBtn = document.querySelector('#proceedActionBtn');
const CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
const transActionModalBody = document.querySelector('#transActionModalBody');
const transActionModalBody_Action = document.querySelector('#transActionModalBody_Action');
const transActionModalTitle = document.querySelector('#transActionModalTitle');
const transUpdateForm = document.querySelector('#transUpdateForm');
const transActionModalAttribute = document.querySelector('#transActionModalAttribute');


actionBtn.forEach((btn)=>{
    btn.addEventListener("click", ()=>{
        let id = btn.getAttribute('data-id');
        let action = btn.getAttribute('data-action');
        let notes = txtAdmin.value;
        transActionModalBody_Action.value = action;
        transActionModalBody_Id.value = id;
        console.log(transActionModalBody_Action.value);
    
        if(action == 'approved'){
            transActionModalTitle.innerHTML = "<strong>" + "Approval Notes" + "</strong>"
        } else if (action == 'declined'){
            transActionModalTitle.innerHTML = "<strong>" + "Decline Notes" + "</strong>"
        } else if (action == 'cancelled'){
            transActionModalTitle.innerHTML = "<strong>" + "Cancellation Notes" + "</strong>"
        } else if (action == 'returned'){
            transActionModalTitle.innerHTML = "<strong>" + "Asset Returned Notes" + "</strong>"
        }
        
        
        let attribute = "/transactions/" + id;
        console.log(attribute);
        transActionModalAttribute.value = attribute;

        transUpdateForm.setAttribute("action", attribute);
        console.log(transUpdateForm.getAttribute('action'));

    
        // let route = "http://localhost:8000/transactions" + "/" + id;
        // console.log(id);
        // console.log(transActionModalBody_Action.value);
        // console.log(route);

        // let formData = new FormData
        // formData.append('action', action);
    
        // const payload = {
        //      method: 'put',
        // //     body: formData,
        //      headers: { 
        //          'X-CSRF-TOKEN' : CSRFToken,
        //          'content-type': 'text/plain'
        //      }
        //  }
    
        //  fetch(route, payload)
        //  .then((res) => {
        //      console.log(res);
        //     //return res.json();
        //     //return res.text();
        //})
        // .then((data) => {
        //     //console.log(data);
        //     //console.log(data.status);
        //     //document.querySelector('#errorCatcher').innerHTML = data;
        //     //location.reload(true);
        // })
        // .catch(error => console.error(error))
    //})
    
        txtAdmin.addEventListener("input", ()=>{
            console.log("some text being entered");
        
            proceedActionBtn.disabled = false;
        
            if (txtAdmin.value == ""){
                errorFlag = true;
                proceedActionBtn.disabled = true;
                //console.log("textarea empty");
            } else {
                errorFlag = false;
            }
        })
    })
})