//    console.log("transdetails connected");
    
    const refNo = document.querySelector('#refNo');
    const modalBody = document.querySelector('#transDetailsModalBody');

    document.querySelectorAll('.transDetailsBtn').forEach((btn)=>{
        btn.addEventListener("click", ()=>{
            document.querySelector('#transDetailsModalBody').innerHTML = "";
            let id = btn.getAttribute('data-id');
            let user = btn.getAttribute('data-user');
            let route = "http://localhost:8000/transactions/" + id;
            // let formData = new FormData

            console.log(id);
            // console.log(user);
            console.log(route);

            // formData.append('id', id);
            // formData.append('user', user);
            // formData.getAll('id', 'user');

            const payload = {
                method: 'GET',
                //body: formData,
                headers: { 
                    'X-CSRF-TOKEN' : CSRFToken
                }
            }

            
            fetch(route, payload)
            .then((res) => {
                //console.log('here at res');
                //console.log(res);
                return res.json();
            })
            .then((data) => {
                //console.log(data.data.req_Notes);
                //console.log(data.data.req_notes);
                if(user == 'User'){
                    
                    document.querySelector('#transDetailsModalBody').innerHTML = data.data.req_notes;
                } else {
                    document.querySelector('#transDetailsModalBody').innerHTML = data.data.admin_notes;
                }
                // document.querySelector('#transDetailsModalTitle').innerHTML =  user + "'s Notes";
                // location.reload(true);
            })
            
                ///--> fetch template for debugging
                // fetch(route, payload)
                // .then(function(response) {
                //     if (!response.ok) {
                //         throw Error(response.statusText);
                //     }
                //     return response;
                // }).then(function(response) {
                //     console.log("ok");
                // }).catch(function(error) {
                //     console.log(error);
                // });
        })
    })

    // document.querySelectorAll('.transDetailsBtn').forEach((btn)=>{
    //     btn.addEventListener("click", ()=>{

    //         let id = btn.getAttribute('data-id');
    //         let user = btn.getAttribute('data-user');
    //         let route = btn.getAttribute('data-route');
    //         let formData = new FormData

    //         console.log(id);
    //         console.log(user);
    //         console.log(route);

    //         formData.append('id', id);
    //         formData.append('user', user);
    //         formData.getAll('id', 'user');

    //         const payload = {
    //             method: 'post',
    //             body: formData,
    //             headers: { 
    //                 'X-CSRF-TOKEN' : CSRFToken 
    //             }
    //         }

    //         fetch(route, payload)
    //         .then((res) => {
    //             //console.log('here at res');
    //             return res.text();
    //         })
    //         .then((data) => {
    //             //console.log('here at data');
    //             document.querySelector('#transDetailsModalBody').innerHTML = data;
    //             document.querySelector('#transDetailsModalTitle').innerHTML =  user + "'s Notes";
    //             //location.reload(true);
    //         })
    //     })
    // })