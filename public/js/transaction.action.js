console.log("yow");

const proceedActionBtn = document.querySelector('#proceedActionBtn');
const transActionModalBody = document.querySelector('#transActionModalBody');
const txtTransActionModalBody_Action = document.querySelector('#transActionModalBody_Action');
const CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
const transActionModalBody_Id = document.querySelector('#transActionModalBody_Id');
const txtAdmin = document.querySelector('#txtAdmin');

document.querySelectorAll('.actionBtn').forEach((btn)=>{
    btn.addEventListener("click", ()=>{
        let id = btn.getAttribute('data-id');
        let action = btn.getAttribute('data-action');

        if (action == 'Approval'){
            function returnValues(){
                return {
                    id: transActionModalBody_Id.value = id,
                    action: txtTransActionModalBody_Action.value = action,
                    title: transActionModalTitle.innerHTML = "<strong>" + "Admin " + "<span style=\"color:green\">" + action +" </span>" + " Notes" + "</strong>"
                    
                }
            }
            values = new returnValues();
            // console.log(values.id);
            // console.log(transActionModalBody_Action.value);
            // console.log(values.title);
        } else if (action == 'Decline') {
            function returnValues(){
                return {
                    id: transActionModalBody_Id.value = id,
                    action: txtTransActionModalBody_Action.value = action,
                    title: transActionModalTitle.innerHTML = "<strong>" + "Admin " + "<span style=\"color:red\">" + action +" </span>" + " Notes" + "</strong>"
                    
                }
            }
            values = new returnValues();
            // console.log(values.id);
            // console.log(values.action);
            // console.log(values.title);
        } else {
            function returnValues(){
                return {
                    id: transActionModalBody_Id.value = id,
                    action: txtTransActionModalBody_Action.value = action,
                    title: transActionModalTitle.innerHTML = "<strong>" + "<span style=\"color:red\">" + action +" </span>" + " Notes" + "</strong>"
                    
                }
            }
            // values = new returnValues();
            // console.log(values.id);
            // console.log(values.action);
            // console.log(values.title);
        }
    })
})

document.querySelectorAll('#proceedActionBtn').forEach((btn)=>{
    btn.addEventListener("click", ()=>{
        //console.log('proceed action button clicked');
        let id = transActionModalBody_Id.value;
        let notes = txtAdmin.value;
        //let action = transActionModalBody_Action.value;
        
        //let action = transActionModalBody_action;
        //let routeExt = "?id=" + id;

        let route = "http://localhost:8000/transactions" + "/" + id;
        //console.log(route);
        let formData = new FormData
        // formData.append('id', id);
        // formData.append('notes', notes);
        formData.append('action', txtTransActionModalBody_Action.value);
        console.log(txtTransActionModalBody_Action.value);
        // formData.getAll('id', 'notes', 'action');

        const payload = {
            method: 'PUT',
            body: formData,
            headers: { 
                'X-CSRF-TOKEN' : CSRFToken
            }
        }

        fetch(route, payload)
        .then((res) => {
            //console.log(res.status);
            //console.log(res.statusText);
            //console.log(res);
            return res.json();
            //return res.text();
        })
        .then((data) => {
            console.log(data);
            //console.log(data.status);
            //document.querySelector('#errorCatcher').innerHTML = data;
            //location.reload(true);
        })
        .catch(error => console.error(error))
    })
})

txtAdmin.addEventListener("input", ()=>{
    //console.log("some text being entered");

    proceedActionBtn.disabled = false;

    if (txtAdmin.value == ""){
        errorFlag = true;
        proceedActionBtn.disabled = true;
        //console.log("textarea empty");
    } else {
        errorFlag = false;
    }
})