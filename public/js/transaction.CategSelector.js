console.log("CategSelector connected");
    
const assetModalBody = document.querySelector('#assetImageModalBody');
const assetCategorySelector = document.querySelector('#txt-assetRental');
const CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');
const transCreate_ImageLoader = document.querySelector('#transCreate_ImageLoader');

assetCategorySelector.addEventListener("change", ()=> {
        
        

        let categ_id = assetCategorySelector.value;
        let route = "http://localhost:8000/transactions/create/" + categ_id;
        let defaultRoute = "http://localhost:8000/transactions/create/";
        console.log(route);
        console.log(assetCategorySelector.value);

        if(assetCategorySelector.value != "reset"){
                const payload = {
                        method: 'GET',
                        //body: formData,
                        headers: { 
                            'X-CSRF-TOKEN' : CSRFToken
                        }
                }
                    
                fetch(route, payload)
                .then((res) => {
                        //console.log('here at res');
                        console.log(res);
                        //return res.text();
                        window.location.assign(route);
                })
        } else if(assetCategorySelector.value == 'reset'){
                window.location.assign(defaultRoute);
        }

        
})