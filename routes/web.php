<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Auth::routes();

route::resource('/assets', 'AssetController');
route::resource('/statuses', 'StatusController');
route::resource('/categories', 'CategoryController');
route::resource('/users', 'UserController');
route::resource('/transactions', 'TransactionController');

route::get('/transactions/create/{id?}', [
    'as' => 'transactions', 'uses' => 'TransactionController@create'
]);

Route::get('/home', 'HomeController@index')->name('home');

route::get('/assetImageGetter/{id}', 'TransactionController@assetImageGetter');
route::get('/categorySelector/{id}', 'TransactionController@categorySelector');


