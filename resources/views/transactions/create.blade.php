@extends('layouts.app')

@include('modals.transCarPickedModal')

@section('content')
<div class="container col-lg-12">
    <div class="row">
        <div class="col-lg-12">

            <h2 class="text-center mb-4">
                {{$title ?? '' }}
            </h2>

            @if (session()->has('status'))

                <div class="alert alert-success" role="alert">
                {{ session()->get('status') }}
                </div>

            @endif
        </div>
    </div>

    <div class="row">
        <div class="col-lg-12">
            <div class="form-group">
                <label for="txt-assetRental">Category</label>
                <select name="txt-assetRental" id="txt-assetRental" class="form-control">
                        <option value="">-- Select a category --</option>
                    @if (count($categories) > 0)
                        @foreach ($categories as $categories)
                            <option value="{{ $categories->id }}">{{ $categories->category_name }}</option>
                        @endforeach
                    @endif
                        <option value="reset">-- Reset category selection --</option>
                </select>
            </div>  
        </div>
    </div>

    @if(count($assets) > 0)
        <div class="row">
            <div class="col-lg-12">
                <div class="row" >
                    @foreach($assets as $asset)
                        <div class="col-lg-4 card-group" id="transCreate_ImageLoader">
                            <div class="card mb-3 border-primary">
                                <div class="card-header">
                                    <h5 class="card-title">{{$asset->name}}</h5>
                                </div>
                                <img src="{{asset($asset->image_path)}}" class="card-img-top border" style="height:160px; width:100%; object-fit: cover;">
                                <div class="card-body text-info">
                                    <h6  class="text-muted mb-3"> Serial No: </h6>
                                    <h6 class="card-subtitle mb-3 ml-3">{{$asset->serialNo}}</h6>
                                    <hr>
                                    <h6  class="text-muted mb-3"> Description: </h6>
                                    <h6 class="card-subtitle mb-3 ml-3">{{$asset->description}}</h6>
                                    <hr>
                                    <h6  class="text-muted mb-3"> Category: </h6>
                                    <h6 class="card-subtitle mb-3 ml-3">{{$asset->category->category_name}}</h6>
                                    <hr>
                                    {{-- @if($asset->isActive == '1')
                                        <small style="color:green">isActive</small>
                                    @else
                                        <small style="color:red">Deactivated</small>
                                    @endif --}}
                                </div>

                                <div class="card-footer text-center">
                                <button type="submit" class="btn btn-primary btn-block transSubmitBtn carPickedBtns" data-toggle="modal" data-target="#transCarPickedModal" data-id="{{$asset->id}}" data-title="{{$asset->name}}">Experience</button>
                                </div>
                            </div>
                        </div>
                    @endforeach
                </div>
            </div>
        </div>
    @else
        <div>
            <h4>No car for rent for this category at the moment. Please contact us later.<h4>
        </div>
    @endif
</div>

    <script type="text/javascript" src="{{ asset('js/transaction.carPicked.js') }}"></script>
    <script type="text/javascript" src="{{ asset('js/transaction.CategSelector.js') }}"></script>
@endsection