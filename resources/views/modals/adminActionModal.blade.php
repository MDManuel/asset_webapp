<!-- Admin's Action Modal -->
<div class="modal fade" id="transActionModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
    <div class="modal-dialog modal-dialog-centered" role="document">
        <div class="modal-content">
        <div class="modal-header">
            <h4 class="modal-title" id="transActionModalTitle"></h4>
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
            </button>
        </div>
        <div class="modal-body" id="transActionModalBody">
        <form method="POST" action="" id="transUpdateForm">
                @csrf
                @method('PUT')
                <input type="hidden" id="transActionModalAttribute" value="">
                <input type="hidden" id="transActionModalBody_Id" value="">
                <input type="hidden" name="transActionModalBody_Action" id="transActionModalBody_Action" value="">
                <textarea class="form-control" id="txtAdmin" name="textAdmin" rows="15" placeholder="Please enter your notes here"></textarea>
                <span id="errorCatcher"></span>
        </div>
        <div class="modal-footer">
                <button type="submit" class="btn btn-success" id="proceedActionBtn">Proceed</button>
                <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
            </form>
        </div>
        
        </div>
    </div>
</div>
    <!-- End Admin's Action Modal -->