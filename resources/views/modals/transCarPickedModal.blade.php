<!-- Admin's Action Modal -->
<div class="modal fade" id="transCarPickedModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalCenterTitle" aria-hidden="true">
  <div class="modal-dialog modal-dialog-centered" role="document">
      <div class="modal-content">
      <div class="modal-header">
          <h4 class="modal-title" id="carPickedModal_title"></h4>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
          </button>
      </div>
      <div class="modal-body" id="carPickedModal_body">
      <form method="POST" action="/transactions" id="transUpdateForm">
              @csrf
              <input type="hidden" id="carPickedModal_Id" name="carPickedModal_Id" value="">

              <div class="form-group">
                <label for="modal_borrowDate">Borrow Date</label>
                <input type="date" class="form-control" id="modal_borrowDate" name="modal_borrowDate" min="" required autocomplete="date" autofocus>
                <small><span id="borrowDateNotif"></span></small>
              </div>

              <div class="form-group">
                <label for="modal_returnDate">Return Date</label>
                <input type="date" class="form-control" id="modal_returnDate" name="modal_returnDate" value="" required autocomplete="date" autofocus>
                <span id="returnDateNotif">Please enter date</span></small>
              </div>

              <div class="form-group">
                  <label for="req_notes">Notes/Special Instructions: </label>
                  <textarea class="form-control" id="req_notes" name="req_notes" rows="10" placeholder="Please enter your notes here" required autocomplete="email" autofocus></textarea>
                  <small><span id="errorCatcher"></span><small>
              </div>
      </div>

      <div class="modal-footer">
              <button  type="submit" class="btn btn-success" id="carPickedBtn" data-id="">Submit</button>  
              <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
          </form>
            
      </div>
      
      </div>
  </div>
</div>
  <!-- End Admin's Action Modal -->