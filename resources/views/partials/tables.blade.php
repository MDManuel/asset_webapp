@can('isAdmin')
                
                    <h3>Pending Requests</h3>

                    <table class="table table-striped table-bordered text-center table-hover">
                        <thead> 
                            <tr>
                                {{-- <th>Image</th> --}}
                                <th>Image</th>
                                <th>Transaction No</th>
                                <th>Requested Asset</th>
                                <th>Requested By</th>
                                <th>Borrow Date</th>
                                <th>Return Date</th>
                                <th>Total Rental Days</th>
                                <th>Requestor's notes</th>
                                <th>Request Status</th>
                                <th>Admin's notes</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Returned At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($transactions as $transaction)
                                @if($transaction->status_id == 1)
                                    <tr class="text-center">
                                        <th>
                                            <img src="{{ asset($transaction->image_path) }}" style="height:100px; width: 160px; object-fit: cover;">
                                            <br><br>
                                            <a href="" class="btn btn-secondary assetDetailsBtn" data-id="{{$transaction->asset_id}}" data-toggle="modal" data-target="#transImageModal">Asset Details</a>
                                        </th>
                                        <th>{{$transaction->refNo}}</th>
                                        <th>{{$transaction->asset->name}}</th>
                                        <th>{{$transaction->user->name}}</th>
                                        <th>{{$transaction->borrowDate}}</th>
                                        <th>{{$transaction->returnDate}}</th>
                                        <td>{{$transaction->totalDays}}</td>
                                        <td>
                                            <button class="btn btn-secondary transDetailsBtn" data-user="User" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transDetailsModal">View</button>
                                        </td>
                                        <td>
                                            {{$transaction->status->status}}
                                        </td>
                                        <td>
                                            <button class="btn btn-secondary transDetailsBtn" data-user="Admin"  data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transDetailsModal"">View</button>
                                        </td>
                                        <td>{{$transaction->created_at}}</td>
                                        <td>{{$transaction->updated_at}}</td>
                                        <td>{{$transaction->clearedDate}}</td>
                                        <td>
                                            @if($transaction->status_id == 1)
                                                {{-- pending request --}}
                                                <div class="actionBtns">
                                                    <button type="button" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transActionModal" data-action="approved" class="btn btn-success btn-block actionBtn">Approve</button>
                                                    <button type="button" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transActionModal" data-action="declined" class="btn btn-danger btn-block actionBtn">Reject</button>
                                                </div>
                                            @elseif($transaction->status_id == 3 || $transaction->status_id == 4 || $transaction->status_id == 5)
                                                {{-- cancelled or returned --}}
                                                <div class="actionBtns">
                                                    <strong>No actions available</strong>
                                                </div>
                                            @elseif($transaction->status_id == 2)
                                                {{-- approved --}}
                                                <div class="actionBtns">
                                                    <button type="button" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transActionModal" data-action="returned" class="btn btn-info btn-block actionBtn">Returned</button>
                                                </div>
                                            @endif 
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>
                    <br><br>

                    <h3>Ongoing Requests</h3>

                    <table class="table table-striped table-bordered text-center table-hover">
                        <thead> 
                            <tr>
                                {{-- <th>Image</th> --}}
                                <th>Image</th>
                                <th>Transaction No</th>
                                <th>Requested Asset</th>
                                <th>Requested By</th>
                                <th>Borrow Date</th>
                                <th>Return Date</th>
                                <th>Total Rental Days</th>
                                <th>Requestor's notes</th>
                                <th>Request Status</th>
                                <th>Admin's notes</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Returned At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($transactions as $transaction)
                                @if($transaction->status_id == 2)
                                    <tr class="text-center">
                                        <th>
                                            <img src="{{ asset($transaction->image_path) }}" style="height:100px; width: 160px; object-fit: cover;">
                                            <br><br>
                                            <a href="" class="btn btn-secondary assetDetailsBtn" data-id="{{$transaction->asset_id}}" data-toggle="modal" data-target="#transImageModal">Asset Details</a>
                                        </th>
                                        <th>{{$transaction->refNo}}</th>
                                        <th>{{$transaction->asset->name}}</th>
                                        <th>{{$transaction->user->name}}</th>
                                        <th>{{$transaction->borrowDate}}</th>
                                        <th>{{$transaction->returnDate}}</th>
                                        <td>{{$transaction->totalDays}}</td>
                                        <td>
                                            <button class="btn btn-secondary transDetailsBtn" data-user="User" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transDetailsModal">View</button>
                                        </td>
                                        <td>
                                            {{$transaction->status->status}}
                                        </td>
                                        <td>
                                            <button class="btn btn-secondary transDetailsBtn" data-user="Admin"  data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transDetailsModal"">View</button>
                                        </td>
                                        <td>{{$transaction->created_at}}</td>
                                        <td>{{$transaction->updated_at}}</td>
                                        <td>{{$transaction->clearedDate}}</td>
                                        <td>
                                            @if($transaction->status_id == 1)
                                                {{-- pending request --}}
                                                <div class="actionBtns">
                                                    <button type="button" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transActionModal" data-action="approved" class="btn btn-success btn-block actionBtn">Approve</button>
                                                    <button type="button" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transActionModal" data-action="declined" class="btn btn-danger btn-block actionBtn">Reject</button>
                                                </div>
                                            @elseif($transaction->status_id == 3 || $transaction->status_id == 4 || $transaction->status_id == 5)
                                                {{-- cancelled or returned --}}
                                                <div class="actionBtns">
                                                    <strong>No actions available</strong>
                                                </div>
                                            @elseif($transaction->status_id == 2)
                                                {{-- approved --}}
                                                <div class="actionBtns">
                                                    <button type="button" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transActionModal" data-action="returned" class="btn btn-info btn-block actionBtn">Returned</button>
                                                </div>
                                            @endif 
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>

                    <br><br>
                    <h3>Cancelled/Declined/Returned Requests</h3>

                    <table class="table table-striped table-bordered text-center table-hover">
                        <thead> 
                            <tr>
                                {{-- <th>Image</th> --}}
                                <th>Image</th>
                                <th>Transaction No</th>
                                <th>Requested Asset</th>
                                <th>Requested By</th>
                                <th>Borrow Date</th>
                                <th>Return Date</th>
                                <th>Total Rental Days</th>
                                <th>Requestor's notes</th>
                                <th>Request Status</th>
                                <th>Admin's notes</th>
                                <th>Created At</th>
                                <th>Updated At</th>
                                <th>Returned At</th>
                                <th>Action</th>
                            </tr>
                        </thead>
                        
                        <tbody>
                            @foreach($transactions as $transaction)
                                @if($transaction->status_id == 3 || $transaction->status_id == 4 || $transaction->status_id == 5)
                                    <tr class="text-center">
                                        <th>
                                            <img src="{{ asset($transaction->image_path) }}" style="height:100px; width: 160px; object-fit: cover;">
                                            <br><br>
                                            <a href="" class="btn btn-secondary assetDetailsBtn" data-id="{{$transaction->asset_id}}" data-toggle="modal" data-target="#transImageModal">Asset Details</a>
                                        </th>
                                        <th>{{$transaction->refNo}}</th>
                                        <th>{{$transaction->asset->name}}</th>
                                        <th>{{$transaction->user->name}}</th>
                                        <th>{{$transaction->borrowDate}}</th>
                                        <th>{{$transaction->returnDate}}</th>
                                        <td>{{$transaction->totalDays}}</td>
                                        <td>
                                            <button class="btn btn-secondary transDetailsBtn" data-user="User" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transDetailsModal">View</button>
                                        </td>
                                        <td>
                                            {{$transaction->status->status}}
                                        </td>
                                        <td>
                                            <button class="btn btn-secondary transDetailsBtn" data-user="Admin"  data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transDetailsModal"">View</button>
                                        </td>
                                        <td>{{$transaction->created_at}}</td>
                                        <td>{{$transaction->updated_at}}</td>
                                        <td>{{$transaction->clearedDate}}</td>
                                        <td>
                                            @if($transaction->status_id == 1)
                                                {{-- pending request --}}
                                                <div class="actionBtns">
                                                    <button type="button" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transActionModal" data-action="approved" class="btn btn-success btn-block actionBtn">Approve</button>
                                                    <button type="button" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transActionModal" data-action="declined" class="btn btn-danger btn-block actionBtn">Reject</button>
                                                </div>
                                            @elseif($transaction->status_id == 3 || $transaction->status_id == 4 || $transaction->status_id == 5)
                                                {{-- cancelled or returned --}}
                                                <div class="actionBtns">
                                                    <strong>No actions available</strong>
                                                </div>
                                            @elseif($transaction->status_id == 2)
                                                {{-- approved --}}
                                                <div class="actionBtns">
                                                    <button type="button" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transActionModal" data-action="returned" class="btn btn-info btn-block actionBtn">Returned</button>
                                                </div>
                                            @endif 
                                        </td>
                                    </tr>
                                @endif
                            @endforeach
                        </tbody>
                    </table>

                
            @else
            <h3>Pending Requests</h3>

            <table class="table table-striped table-bordered text-center table-hover">
                <thead> 
                    <tr>
                        {{-- <th>Image</th> --}}
                        <th>Image</th>
                        <th>Transaction No</th>
                        <th>Requested Asset</th>
                        <th>Borrow Date</th>
                        <th>Return Date</th>
                        <th>Total Rental Days</th>
                        <th>My notes</th>
                        <th>Request Status</th>
                        <th>Admin's notes</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach($transactions as $transaction)
                        @if($transaction->status_id == 1)
                            <tr class="text-center">
                                <th>
                                    <img src="{{ asset($transaction->image_path) }}" style="height:100px; width: 160px; object-fit: cover;">
                                    <br><br>
                                    <a href="" class="btn btn-secondary assetDetailsBtn" data-id="{{$transaction->asset_id}}" data-toggle="modal" data-target="#transImageModal">Asset Details</a>
                                </th>
                                <th>{{$transaction->refNo}}</th>
                                <th>{{$transaction->asset->name}}</th>
                                <th>{{$transaction->borrowDate}}</th>
                                <th>{{$transaction->returnDate}}</th>
                                <td>{{$transaction->totalDays}}</td>
                                <td>
                                    <button class="btn btn-secondary transDetailsBtn" data-user="User" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transDetailsModal">View</button>
                                </td>
                                <td>
                                    {{$transaction->status->status}}
                                </td>
                                <td>
                                    <button class="btn btn-secondary transDetailsBtn" data-user="Admin"  data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transDetailsModal"">View</button>
                                </td>
                                <td>
                                    @if($transaction->status_id == 1)
                                        {{-- pending request --}}
                                        <div class="actionBtns">
                                            <button type="button" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transActionModal" data-action="cancelled" class="btn btn-warning btn-block actionBtn">Cancel</button>
                                        </div>
                                    @elseif($transaction->status_id == 3 || $transaction->status_id == 4 || $transaction->status_id == 5)
                                            {{-- cancelled or returned --}}
                                        <div class="actionBtns">
                                            <strong>No actions available</strong>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
            <br><br>

            <h3>Ongoing Requests</h3>

            <table class="table table-striped table-bordered text-center table-hover">
                <thead> 
                    <tr>
                        {{-- <th>Image</th> --}}
                        <th>Image</th>
                        <th>Transaction No</th>
                        <th>Requested Asset</th>
                        <th>Borrow Date</th>
                        <th>Return Date</th>
                        <th>Total Rental Days</th>
                        <th>My notes</th>
                        <th>Request Status</th>
                        <th>Admin's notes</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach($transactions as $transaction)
                        @if($transaction->status_id == 2)
                            <tr class="text-center">
                                <th>
                                    <img src="{{ asset($transaction->image_path) }}" style="height:100px; width: 160px; object-fit: cover;">
                                    <br><br>
                                    <a href="" class="btn btn-secondary assetDetailsBtn" data-id="{{$transaction->asset_id}}" data-toggle="modal" data-target="#transImageModal">Asset Details</a>
                                </th>
                                <th>{{$transaction->refNo}}</th>
                                <th>{{$transaction->asset->name}}</th>
                                <th>{{$transaction->borrowDate}}</th>
                                <th>{{$transaction->returnDate}}</th>
                                <td>{{$transaction->totalDays}}</td>
                                <td>
                                    <button class="btn btn-secondary transDetailsBtn" data-user="User" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transDetailsModal">View</button>
                                </td>
                                <td>
                                    {{$transaction->status->status}}
                                </td>
                                <td>
                                    <button class="btn btn-secondary transDetailsBtn" data-user="Admin"  data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transDetailsModal"">View</button>
                                </td>
                                <td>
                                    @if($transaction->status_id == 1)
                                        {{-- pending request --}}
                                        <div class="actionBtns">
                                            <button type="button" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transActionModal" data-action="cancelled" class="btn btn-warning btn-block actionBtn">Cancel</button>
                                        </div>
                                    @elseif($transaction->status_id == 3 || $transaction->status_id == 4 || $transaction->status_id == 5)
                                            {{-- cancelled or returned --}}
                                        <div class="actionBtns">
                                            <strong>No actions available</strong>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>

            <br><br>
            <h3>Cancelled/Declined/Returned Requests</h3>

            <table class="table table-striped table-bordered text-center table-hover">
                <thead> 
                    <tr>
                        {{-- <th>Image</th> --}}
                        <th>Image</th>
                        <th>Transaction No</th>
                        <th>Requested Asset</th>
                        <th>Borrow Date</th>
                        <th>Return Date</th>
                        <th>Total Rental Days</th>
                        <th>My notes</th>
                        <th>Request Status</th>
                        <th>Admin's notes</th>
                        <th>Action</th>
                    </tr>
                </thead>
                
                <tbody>
                    @foreach($transactions as $transaction)
                        @if($transaction->status_id == 3 || $transaction->status_id == 4 || $transaction->status_id == 5)
                            <tr class="text-center">
                                <th>
                                    <img src="{{ asset($transaction->image_path) }}" style="height:100px; width: 160px; object-fit: cover;">
                                    <br><br>
                                    <a href="" class="btn btn-secondary assetDetailsBtn" data-id="{{$transaction->asset_id}}" data-toggle="modal" data-target="#transImageModal">Asset Details</a>
                                </th>
                                <th>{{$transaction->refNo}}</th>
                                <th>{{$transaction->asset->name}}</th>
                                <th>{{$transaction->borrowDate}}</th>
                                <th>{{$transaction->returnDate}}</th>
                                <td>{{$transaction->totalDays}}</td>
                                <td>
                                    <button class="btn btn-secondary transDetailsBtn" data-user="User" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transDetailsModal">View</button>
                                </td>
                                <td>
                                    {{$transaction->status->status}}
                                </td>
                                <td>
                                    <button class="btn btn-secondary transDetailsBtn" data-user="Admin"  data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transDetailsModal"">View</button>
                                </td>
                                <td>
                                    @if($transaction->status_id == 1)
                                        {{-- pending request --}}
                                        <div class="actionBtns">
                                            <button type="button" data-id="{{$transaction->id}}" data-toggle="modal" data-target="#transActionModal" data-action="cancelled" class="btn btn-warning btn-block actionBtn">Cancel</button>
                                        </div>
                                    @elseif($transaction->status_id == 3 || $transaction->status_id == 4 || $transaction->status_id == 5)
                                            {{-- cancelled or returned --}}
                                        <div class="actionBtns">
                                            <strong>No actions available</strong>
                                        </div>
                                    @endif
                                </td>
                            </tr>
                        @endif
                    @endforeach
                </tbody>
            </table>
            @endcan