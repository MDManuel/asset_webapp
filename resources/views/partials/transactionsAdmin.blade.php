<?php use Carbon\Carbon; ?>

@include('modals.requestorNotesModal')

@include('modals.adminActionModal')

@include('modals.transactionImageModal')

<div class="container-fluid">
    <h2 class="text-center mb-4">{{$title}}</h2>

    <div class="row">
        <div class="col-lg-12">
            @if (session()->has('status'))
                <div class="alert alert-success" role="alert">
                {{ session()->get('status') }}
                </div>
            @endif

            @include('partials.tables')
            
        </div>
    </div>
</div>

<script type="text/javascript" src="{{ asset('js/transaction.approve.js') }}"></script>

<script type="text/javascript" src="{{ asset('js/transDetails.js')}}"></script>

<script type="text/javascript" src="{{ asset('js/transAssetDetails.js')}}"></script>