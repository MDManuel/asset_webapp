@extends('layouts.app')

@section('content')
    <h2 class="text-center mb-4">{{$title}}*</h2>

    {{-- {{$status->id}} --}}

    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            @if (session()->has('status'))

                <div class="alert alert-success" role="alert">
                {{ session()->get('status') }}
                </div>
            @endif

            <table class="table table-striped table-bordered" id="t able-list-users">
                <thead> 
                    <tr>
                        <th>Name</th>
                        <th>Email Address</th>
                        <th>isAdmin</th>
                        <th>created_at</th>
                        <th>updated_at</th>
                        <th></th>
                    </tr>
                </thead>

                @foreach($users as $user)
                <tbody>
                    <tr>
                        <td>{{$user->name}}</td>
                        <td>{{$user->email }}</td>
                        @if($user->isAdmin == 1)
                            <td>Admin</td>
                        @else
                            <td>User</td>
                        @endif

                        {{-- @if($user->isActive == 1)
                            <td>Active</td>
                        @else
                            <td>Deactivated</td>
                        @endif  --}}
                        <td>{{$user->created_at}}</td>
                        <td>{{$user->updated_at}}</td>
                        <td>
                            <a class="btn btn-secondary btn-block editBtn" href="/users/{{$user->id}}/edit">Edit</a>
                            {{-- <form method="POST" action="/users/{{$user->id}}">
                                <div class="btn-group btn-block">
                                    @csrf
                                    @method('DELETE')
                                    @if($user->isActive == true)
                                        <button type="submit" class="btn btn-danger deleteBtn">Deactivate</button>
                                    @else
                                        <button type="submit" class="btn btn-info deleteBtn">Activate</button>
                                    @endif
                                </div>
                            </form> --}}
                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>
            *<small>NOTE: To create a new user profile or change password, you may do so at login page.</small>
        </div>
    </div>

    

    {{-- <script>

        const CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        document.querySelectorAll('.editBtn').forEach((btn)=>{
            btn.addEventListener("click", ()=>{
                console.log("script")
                let id = btn.getAttribute('data-id');
                $url = route('StatusController@edit', ['id' => $id]);
                console.log($url);
                //btn.parentElement.dataset.id
                console.log($url);
                const route = ($url)
                const payload = {
                    method: 'GET',
                    headers: { 
                        'X-CSRF-TOKEN' : CSRFToken 
                    }
                }

                fetch(route, payload)
                .then((res) => {
                    console.log(res);
                    return res.text()
                })
                .then((data) => {
                    console.log(data);
                    // 'txtCategories.innerHTML += data
                })
            })
        })
    </script> --}}

    

@endsection