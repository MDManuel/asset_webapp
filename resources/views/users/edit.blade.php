@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            @if (session()->has('status'))

                <div class="alert alert-success" role="alert">
                {{ session()->get('status') }}
                </div>
            @endif

           
            <h3>Edit User</h3>

            <form method="POST" action="/users/{{$user->id}}">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" id="txt-name" class="form-control" value="{{$user->name}}" required autocomplete="name" autofocus>
                </div>
                <div class="form-group">
                    <label for="name">Email Address:</label>
                    <input type="text" name="email" id="txt-email" class="form-control" value="{{$user->email}}" required autocomplete="email" autofocus>
                </div>
                    <button type="submit" class="btn btn-success form-control" id="btn-edit-user">Update User</button>
                    <a type="button" class="btn btn-warning form-control" href="/users">Cancel</a>
            </form>
        </div>
    </div>
@endsection