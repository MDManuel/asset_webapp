@extends('layouts.app')

@section('content')
    <h2 class="text-center mb-4">{{$title}}</h2>

    {{-- {{$status->id}} --}}

    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            @if (session()->has('status'))

                <div class="alert alert-success" role="alert">
                {{ session()->get('status') }}
                </div>
            @endif

           
            <h3>
                Create New Category
                <a class="btn btn-sm btn-success" data-toggle="collapse" href="#div-add-categories">+</a>
            </h3>

            <form method="POST" action="/categories" id="div-add-categories" class="collapse mb-4">
                @csrf
                <div class="form-group">
                    <label for="name">Category Name:</label>
                    <input type="text" name="category" id="txt-category" class="form-control" required autocomplete="category" autofocus>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success form-control" id="btn-add-category">Add Category</button>
                </div>
            </form>
            
            <hr>

            <h3>
                Categories List
            </h3>

            <table class="table table-striped table-bordered" id="table-list-categories">
                <thead> 
                    <tr>
                        <th>Category Name</th>
                        <th>created_at</th>
                        <th>updated_at</th>
                        <th></th>
                    </tr>
                </thead>

                @foreach($categories as $category)
                <tbody>
                    <tr>
                        <td>{{$category->category_name}}</td>
                        <td>{{$category->created_at}}</td>
                        <td>{{$category->updated_at}}</td>
                        <td>
                            <a class="btn btn-secondary btn-block editBtn" href="/categories/{{$category->id}}/edit">Edit</a>
                            {{-- <form method="POST" action="/categories/{{$category->id}}">
                                <div class="btn-group btn-block">
                                    @csrf
                                    @method('DELETE')
                                    @if($category->isActive == true)
                                        <button type="submit" class="btn btn-danger deleteBtn">Deactivate</button>
                                    @else
                                        <button type="submit" class="btn btn-info deleteBtn">Activate</button>
                                    @endif
                                </div>
                            </form> --}}
                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
    </div>

    

    {{-- <script>

        const CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        document.querySelectorAll('.editBtn').forEach((btn)=>{
            btn.addEventListener("click", ()=>{
                console.log("script")
                let id = btn.getAttribute('data-id');
                $url = route('StatusController@edit', ['id' => $id]);
                console.log($url);
                //btn.parentElement.dataset.id
                console.log($url);
                const route = ($url)
                const payload = {
                    method: 'GET',
                    headers: { 
                        'X-CSRF-TOKEN' : CSRFToken 
                    }
                }

                fetch(route, payload)
                .then((res) => {
                    console.log(res);
                    return res.text()
                })
                .then((data) => {
                    console.log(data);
                    // 'txtCategories.innerHTML += data
                })
            })
        })
    </script> --}}

    

@endsection