@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            @if (session()->has('status'))

                <div class="alert alert-success" role="alert">
                {{ session()->get('status') }}
                </div>
            @endif

           
            <h3>Edit Category</h3>

            <form method="POST" action="/categories/{{$category->id}}">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">Category Name:</label>
                    <input type="text" name="category" id="txt-category" class="form-control" value="{{$category->category_name}}" required autocomplete="category" autofocus>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success form-control" id="btn-add-category">Update Category</button>
                    <a type="button" class="btn btn-warning form-control" href="/categories">Cancel</a>
                </div>
            </form>
        </div>
    </div>
@endsection