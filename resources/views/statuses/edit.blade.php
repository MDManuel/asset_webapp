@extends('layouts.app')


@section('content')
    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            @if (session()->has('status'))

                <div class="alert alert-success" role="alert">
                {{ session()->get('status') }}
                </div>
            @endif

           
            <h3>Edit Status</h3>

            <form method="POST" action="/statuses/{{$status->id}}">
                @csrf
                @method('PUT')
                <div class="form-group">
                    <label for="name">New Status:</label>
                    <input type="text" name="status" id="txt-status" class="form-control" value="{{$status->status}}" required autocomplete="email" autofocus>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success form-control" id="btn-add-category">Update Status</button>
                    <a type="button" class="btn btn-warning form-control" href="/statuses">Cancel</a>
                </div>
            </form>
        </div>
    </div>
@endsection