@extends('layouts.app')

@section('content')
    <h2 class="text-center mb-4">{{$title}}</h2>

    {{-- {{$status->id}} --}}

    <div class="row">
        <div class="col-lg-8 offset-lg-2">
            @if (session()->has('status'))

                <div class="alert alert-success" role="alert">
                {{ session()->get('status') }}
                </div>
            @endif

           
            <h3>
                Create New Status*
                <a class="btn btn-sm btn-success" data-toggle="collapse" href="#div-add-statuses">+</a>
            </h3>

            <form method="POST" action="/statuses" id="div-add-statuses" class="collapse mb-4">
                @csrf
                <div class="form-group">
                    <label for="name">Status:</label>
                    <input type="text" name="status" id="txt-status" class="form-control" required autocomplete="email" autofocus>
                </div>
                <div class="form-group">
                    <button type="submit" class="btn btn-success form-control" id="btn-add-category">Add Status</button>
                </div>
            </form>
            
            <hr>

            <h3>
                Statuses List
            </h3>

            <table class="table table-striped table-bordered" id="table-list-statuses">
                <thead> 
                    <tr>
                        <th>Status</th>
                        <th>created_at</th>
                        <th>updated_at</th>
                        <th></th>
                    </tr>
                </thead>

                @foreach($statuses as $status)
                <tbody>
                    <tr>
                        <td>{{$status->status}}</td>
                        <td>{{$status->created_at}}</td>
                        <td>{{$status->updated_at}}</td>
                        <td>
                            <a class="btn btn-secondary btn-block editBtn" href="/statuses/{{$status->id}}/edit">Edit</a>
                            {{-- <form method="POST" action="/statuses/{{$status->id}}">
                                <div class="btn-group btn-block">
                                    @csrf
                                    @method('DELETE')
                                    @if($status->isActive == true)
                                        <button type="submit" class="btn btn-danger deleteBtn">Deactivate</button>
                                    @else
                                        <button type="submit" class="btn btn-info deleteBtn">Activate</button>
                                    @endif
                                </div>
                            </form> --}}
                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>
            <small>*To create new table in the transactions dashboard for the newly created status, please contact the developer.</small>
        </div>
    </div>

    

    {{-- <script>

        const CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        document.querySelectorAll('.editBtn').forEach((btn)=>{
            btn.addEventListener("click", ()=>{
                console.log("script")
                let id = btn.getAttribute('data-id');
                $url = route('StatusController@edit', ['id' => $id]);
                console.log($url);
                //btn.parentElement.dataset.id
                console.log($url);
                const route = ($url)
                const payload = {
                    method: 'GET',
                    headers: { 
                        'X-CSRF-TOKEN' : CSRFToken 
                    }
                }

                fetch(route, payload)
                .then((res) => {
                    console.log(res);
                    return res.text()
                })
                .then((data) => {
                    console.log(data);
                    // 'txtCategories.innerHTML += data
                })
            })
        })
    </script> --}}

    

@endsection