@extends('layouts.app')

@section('content')

    <a class="btn btn-secondary ml-4" href="/assets">Back</a>

    <h2 class="text-center mb-4">{{$asset->name}} Details</h2>
        
    <div class="row">
        <div class="col-lg-4 offset-lg-2">
            
            <form method="post" action="/assets" enctype="multipart/form-data">
                <div class="form-group">
                    <label for="name">Name:</label>
                <input disabled type="text" name="name" id="name" class="form-control" value="{{$asset->name}}">
                </div>

                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea disabled name="description" id="description" class="form-control">{{$asset->description}}</textarea>
                </div>

                <div class="form-group">
                    <label for="stocks">Stocks:</label>
                    <input disabled type="number" name="stocks" id="stocks" class="form-control" value="{{$asset->stocks}}">
                </div>

                <div class="form-group">
                    <label for="category">Category:</label>
                    <input disabled type="text" name="category" class="form-control" value="{{$asset->category->category_name}}">
                </div>

                <div class="form-group">
                    <label for="isActive">IsActive</label>
                    <input disabled type="text" name="isActive" id="isActive" class="form-control" value="{{$asset->isActive}}">
                </div>

                <div class="form-group">
                    <label for="created_at">Created At:</label>
                    <input disabled type="text" name="created_at" id="created_at" class="form-control" value="{{$asset->created_at}}">
                </div>
    
                <div class="form-group">
                    <label for="updated_at">Updated At:</label>
                    <input disabled type="text" name="updated_at" id="updated_at" class="form-control" value="{{$asset->updated_at}}">
                </div>
            </form>
        </div>
        
                <div class="col-lg-3 m-0 mx-auto p-0">
                    <img src="{{ asset($asset->image_path) }}" style="height:250px; widht:auto; object-fit: cover;">
                    <br><br>
                    <a class="btn btn-primary btn-block editBtn" href="/assets/{{$asset->id}}/edit">Edit</a>
                    <form method="POST" action="/assets/{{$asset->id}}">
                        <div class="btn-group btn-block">
                            @csrf
                            @method('DELETE')
                            @if($asset->isActive == true)
                                <button type="submit" class="btn btn-danger deleteBtn">Deactivate</button>
                            @else
                                <button type="submit" class="btn btn-info deleteBtn">Activate</button>
                            @endif
                        </div>
                    </form>
                </div>
            
        </div>
    </div>          
@endsection