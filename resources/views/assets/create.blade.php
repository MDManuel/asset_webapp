@extends('layouts.app')

@section('content')

   
    <div class="row mx-auto">
        
        <div class="col-lg-8 offset-lg-2">
            <h2 class="text-center mb-4">
                    {{$title}}
            </h2>
        </div>

        <div class="col-lg-8 offset-lg-2">

            <h4>Add New Asset</h4>

            @if (session()->has('status'))

                <div class="alert alert-success" role="alert">
                  {{ session()->get('status') }}
                </div>

            @endif

            <form method="POST" action="/assets" enctype="multipart/form-data">

                @csrf

                <div class="form-group">
                    <label for="name">Name:</label>
                    <input type="text" name="name" id="name" class="form-control" required autocomplete="on" autofocus>
                </div>

                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea name="description" id="description" class="form-control" rows="13" required autocomplete="on" autofocus></textarea>
                </div>

                <div class="form-group">
                    <label for="image">Upload image:</label>
                    <input type="file" name="image" id="image" class="form-control" >
                </div>

                <div class="form-group">
                    <label for="category">Category:</label>
                    <select name="category" id="txt-categories" class="form-control" required autocomplete="on" autofocus>
                        @if (count($categories) > 0)
                            @foreach ($categories as $category)
                                <option value="{{ $category->id }}">{{ $category->category_name }}</option>
                            @endforeach
                        @endif
                    </select>
                </div>  

                <button type="submit" class="btn btn-success">Add Asset</button>
                <a class="btn btn-warning" href="/asset">Cancel</a>

            </form>

        </div>

    </div>

@endsection