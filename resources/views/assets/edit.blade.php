@extends('layouts.app')

@section('content')

    <a class="btn btn-secondary ml-4" href="/assets">Back</a>

    <h2 class="text-center mb-4">{{$asset->name}} Details : UPDATE </h2>
        
    <div class="row">
        <div class="col-lg-4 offset-lg-2">

            @if (session()->has('status'))

                <div class="alert alert-success" role="alert">
                    {{ session()->get('status') }}
                </div>

            @endif
            
            <form method="POST" action="/assets/{{$asset->id}}" enctype="multipart/form-data">
                @csrf
                @method('PUT')
                
                <div class="form-group">
                    <label for="name">Name:</label>
                <input type="text" name="name" id="name" class="form-control" value="{{$asset->name}}" required autocomplete="name" autofocus>
                </div>

                <div class="form-group">
                    <label for="description">Serial No:</label>
                    <textarea disabled name="serialNo" id="serialNo" class="form-control" required autocomplete="name" autofocus>{{$asset->serialNo}}</textarea>
                </div>

                <div class="form-group">
                    <label for="description">Description:</label>
                    <textarea name="description" id="description" class="form-control" required autocomplete="name" autofocus>{{$asset->description}}</textarea>
                </div>

                <div class="form-group">
                    <label for="category">Category:</label>
                    <select name="category" id="txt-categories" class="form-control" required autocomplete="name" autofocus>
                        @if (count($categories) > 0)
                            @foreach ($categories as $category)
                                @if($category->id == $asset->category_id)
                                    <option value="{{ $category->id }}" selected>{{ $category->category_name }}</option>
                                @else
                                     <option value="{{ $category->id }}">{{ $category->category_name }}</option>                            
                                @endif
                            @endforeach
                        @endif
                    </select>
                </div>  

                <div class="form-group">
                    <label for="isActive">IsActive</label>
                    <input  disabled type="text" name="isActive" id="isActive" class="form-control " value="{{$asset->isActive}}">
                </div>
        </div>
                <div class="col-lg-3 m-0 mx-auto p-0">
                    <img src="{{ asset($asset->image_path) }}" style="height:250px; widht:auto; object-fit: cover;">
                    <div class="form-group">
                        <label for="image">Upload image:</label>
                        <input type="file" name="image" id="image" class="form-control" value="{{$asset->image_path}}">
                    </div>
                    <br><br>
                    <button type="submit" class="btn btn-info btn-block editBtn">Update</button>
            </form>
                    <form method="POST" action="/assets/{{$asset->id}}">
                        <div class="btn-group btn-block">
                            @csrf
                            @method('DELETE')
                            @if($asset->isActive == true)
                                <button type="submit" class="btn btn-danger deleteBtn">Deactivate</button>
                            @else
                                <button type="submit" class="btn btn-info deleteBtn">Activate</button>
                            @endif
                        </div>
                    </form>
                    <a type="submit" class="btn btn-warning btn-block editBtn" href="/assets">Cancel</a>
                </div>
        </div>
    </div>          
@endsection