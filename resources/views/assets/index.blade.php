@extends('layouts.app')

@section('content')
    <h2 class="text-center mb-4">{{$title}}</h2>

    <div class="row">
        <div class="col-lg-12>
            @if (session()->has('status'))

                <div class="alert alert-success" role="alert">
                {{ session()->get('status') }}
                </div>
            @endif
            <br><br>
            <h3>
                Assets List
                <br>
                <a class="btn btn-sm btn-success" href="/assets/create">Add New Asset</a>
            </h3>

            <table class="table table-striped table-bordered text-center">
                <thead> 
                    <tr>
                        <th>Image</th>
                        <th>Name</th>
                        <th>Serial No</th>
                        <th>Description</th>
                        <th>isActive</th>
                        <th>Category</th>
                        <th>Action</th>
                    </tr>
                </thead>

                @foreach($assets as $asset)
                <tbody>
                    <tr>
                        <th><img src="{{ asset($asset->image_path) }}" style="height:150px; width: 200px; object-fit: cover;"></th>
                        <th><a href="assets/{{$asset->id}}">{{$asset->name}}</a></th>
                        <th>{{$asset->serialNo}}</th>
                        <th>{{$asset->description}}</th>
                        <th>{{$asset->isActive}}</th>
                        <th>{{$asset->category->category_name}}</th>
                        <td>
                            <a class="btn btn-primary btn-block editBtn" href="/assets/{{$asset->id}}/edit">Edit</a>
                            <form method="POST" action="/assets/{{$asset->id}}">
                                <div class="btn-group btn-block">
                                    @csrf
                                    @method('DELETE')
                                    @if($asset->isActive == true)
                                        <button type="submit" class="btn btn-danger deleteBtn">Deactivate</button>
                                    @else
                                        <button type="submit" class="btn btn-info deleteBtn">Activate</button>
                                    @endif
                                </div>
                            </form>
                        </td>
                    </tr>
                </tbody>
                @endforeach
            </table>
        </div>
    </div>

    

    {{-- <script>

        const CSRFToken = document.querySelector('meta[name="csrf-token"]').getAttribute('content');

        document.querySelectorAll('.editBtn').forEach((btn)=>{
            btn.addEventListener("click", ()=>{
                console.log("script")
                let id = btn.getAttribute('data-id');
                $url = route('StatusController@edit', ['id' => $id]);
                console.log($url);
                //btn.parentElement.dataset.id
                console.log($url);
                const route = ($url)
                const payload = {
                    method: 'GET',
                    headers: { 
                        'X-CSRF-TOKEN' : CSRFToken 
                    }
                }

                fetch(route, payload)
                .then((res) => {
                    console.log(res);
                    return res.text()
                })
                .then((data) => {
                    console.log(data);
                    // 'txtCategories.innerHTML += data
                })
            })
        })
    </script> --}}

    

@endsection