<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateTransactionsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('transactions', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('image_path');
            $table->string('refNo');
            $table->unsignedBigInteger('asset_id');
            $table->string('serialNo');
            $table->unsignedBigInteger('user_id');
            $table->dateTime('borrowDate');
            $table->dateTime('returnDate');
            $table->integer('totalDays')->default(0);
            $table->mediumText('req_notes');
            $table->unsignedBigInteger('status_id')->default(1);
            $table->mediumText('admin_notes');
            $table->dateTime('clearedDate')->nullable();
            $table->timestamps();
            //set the properties of user_id foreign key
            $table->foreign('user_id')
            ->references('id')
            ->on('users')
            ->onDelete('restrict')
            ->onUpdate('cascade');
            //set properties of status_id foreign key
            $table->foreign('status_id')
            ->references('id')
            ->on('statuses')
            ->onDelete('restrict')
            ->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('transactions');
    }
}
