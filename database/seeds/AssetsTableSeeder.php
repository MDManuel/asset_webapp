<?php

use Illuminate\Database\Seeder;

class AssetsTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('assets')->delete();
        
        \DB::table('assets')->insert(array (
            0 => 
            array (
                'id' => 1,
                'serialNo' => '1024_2-Seater_1',
                'name' => '2018 Mercedes-Benz SLC',
                'description' => 'The 2018 Mercedes SLC comes in two distinct flavors: the SLC 300, with a 241-horsepower turbocharged four-cylinder engine, and the AMG SLC 43, with an eager 362-horsepower twin-turbo V6. The AMG model can propel the roadster from zero to 60 mph in 4.6 seconds.',
                'image_path' => 'images/1571903908_1.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2019-10-24 02:15:07',
                'updated_at' => '2019-10-24 07:58:28',
            ),
            1 => 
            array (
                'id' => 2,
                'serialNo' => '1024_2-Seater_2',
                'name' => '2018 Audi TT Roadster',
                'description' => 'The TT Roadster packs a 220-horsepower four-cylinder engine. It isn’t as strong as some rivals’ engines, but it’s peppy enough to get you around town. It comes standard with Audi’s Virtual Cockpit, a large, customizable display screen that replaces a traditional instrument panel. Leather upholstery also comes standard.',
                'image_path' => 'images/1571884000.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2019-10-24 02:26:40',
                'updated_at' => '2019-10-24 02:26:40',
            ),
            2 => 
            array (
                'id' => 3,
                'serialNo' => '1024_LuxCars_3',
                'name' => '2019 Jaguar F-Type',
                'description' => 'The 2019 Jaguar F-Type announces itself with a rumbling exhaust note and supercar looks. There are five engine options, ranging from a 296-horsepower turbocharged four-cylinder to a pair of commanding, supercharged V8s. If you want excellent fuel economy, get the four-cylinder. For fun, opt for the V6 or V8 options.',
                'image_path' => 'images/1571884641.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2019-10-24 02:37:21',
                'updated_at' => '2019-10-24 02:37:21',
            ),
            3 => 
            array (
                'id' => 4,
                'serialNo' => '1024_LuxCars_4',
                'name' => '2019 Fiat 124 Spider',
                'description' => 'The 2019 Fiat 124 Spider proves you don’t need several hundred horsepower to have a good time behind the wheel. The lightweight two-seat roadster is fitted with either a 160 or 164-horsepower turbocharged four-cylinder engine. A six-speed manual transmission comes standard, while a six-speed automatic is available.',
                'image_path' => 'images/1571884942.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2019-10-24 02:42:22',
                'updated_at' => '2019-10-24 02:42:23',
            ),
            4 => 
            array (
                'id' => 5,
                'serialNo' => '1024_LuxCars_5',
                'name' => '2018 Acura NSX',
                'description' => 'The 2019 Acura NSX features a wildly complex gas-electric hybrid powertrain that provides world-class performance. Its three electric motors and twin-turbo V6 work together like a symphony to produce a total of 573-horsepower. That power flows to each of the 2-seat coupe’s four wheels, as needed, for maximum performance.',
                'image_path' => 'images/1571885025.jpg',
                'isActive' => 1,
                'category_id' => 1,
                'created_at' => '2019-10-24 02:43:45',
                'updated_at' => '2019-10-24 02:43:45',
            ),
            5 => 
            array (
                'id' => 6,
                'serialNo' => '1024_Pickup_6',
                'name' => 'Ford F-150',
                'description' => 'The Ford F-150 has been the best-selling vehicle in the U.S. for decades, and it’s easy to see why legions of truck buyers flock to it every year. The F-150 doesn’t dominate any particular category, but it covers all of the bases with solid performance, impressive refinement, and stylish and thoughtful design features.',
                'image_path' => 'images/1571885213.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2019-10-24 02:46:53',
                'updated_at' => '2019-10-24 02:46:53',
            ),
            6 => 
            array (
                'id' => 7,
                'serialNo' => '1024_Pickup_7',
                'name' => 'GMC Sierra 1500 Denali',
                'description' => 'At this point, most manufacturers are adding wood leather trim to their pickup trucks to create luxury models, but the product generally doesn’t live up to the “luxury” branding, or the inflated price tags most of these models carry. That’s not the case with the GMC Sierra 1500 Denali, which offers buyers more than just the garnish of luxury. GMC’s designers continue to take a more tasteful approach than their counterparts at other truck makers, giving Denali models a true upscale feel.',
                'image_path' => 'images/1571902907_4.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2019-10-24 02:47:48',
                'updated_at' => '2019-10-24 07:41:47',
            ),
            7 => 
            array (
                'id' => 8,
                'serialNo' => '1024_Pickup_8',
                'name' => 'Jeep Gladiator',
                'description' => 'Most four-wheel drive trucks are decent off-road, but the Jeep Gladiator is better than the rest because it’s built using the Wrangler‘s trail-tested components. Its natural habitat is a dirt trail miles away from the nearest paved road, not a construction site. To that end, it comes exclusively with a four-door cab placed in front of a 60-inch cargo box, and Jeep doesn’t offer a stripped-down, no-frills trim like many of its rivals do.',
                'image_path' => 'images/1571885339.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2019-10-24 02:48:59',
                'updated_at' => '2019-10-24 02:48:59',
            ),
            8 => 
            array (
                'id' => 9,
                'serialNo' => '1024_Pickup_9',
                'name' => 'Honda Ridgeline',
                'description' => '"The best truck for people who don’t like trucks" The Ridgeline is different from most other trucks on the market. It’s basically a Honda Pilot crossover with a pickup bed, so it uses car-like unibody construction instead of the body-on-frame construction of most trucks. That means the body is more rigid, which makes for better ride quality, with less shuddering and vibration. It’s also a little bit easier to drive and park.',
                'image_path' => 'images/1571904059_4.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2019-10-24 02:50:29',
                'updated_at' => '2019-10-24 08:00:59',
            ),
            9 => 
            array (
                'id' => 10,
                'serialNo' => '1024_Pickup_10',
                'name' => 'Chevrolet Colorado Diesel',
                'description' => 'Diesel may be getting a bad rap in the wake of the Volkswagen emissions scandal, but when automakers don’t cheat, it still makes a lot of sense in new vehicles. That’s the case with trucks, both because diesel engines’ torque is good for towing, and because there are no real hybrid or electric alternatives in this segment for buyers looking for low fuel costs and emissions.',
                'image_path' => 'images/1571885508.jpg',
                'isActive' => 1,
                'category_id' => 4,
                'created_at' => '2019-10-24 02:51:48',
                'updated_at' => '2019-10-24 02:51:48',
            ),
            10 => 
            array (
                'id' => 11,
                'serialNo' => '1024_Limousine_11',
                'name' => 'Toyota Century Royal',
                'description' => 'Like the Cadillac One, the Century Royal has one user only and no commercial car service can buy one. The Century Royal is made strictly for the Emperor of Japan, and Toyota has built only four since 2006. Like the S Class, the Century Royal has a twelve-cylinder engine.',
                'image_path' => 'images/1571886962_5.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2019-10-24 02:53:26',
                'updated_at' => '2019-10-24 03:16:02',
            ),
            11 => 
            array (
                'id' => 12,
                'serialNo' => '1024_Limousine_12',
                'name' => 'Rolls-Royce Phantom EWB',
                'description' => 'Rolls-Royce and luxury have been synonymous with each other for a long time. This Rolls-Royce Phantom continues a line that premiered as the first Rolls-Royce to be rolled out after the legendary brand was taken over by BMW.',
                'image_path' => 'images/1571885651.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2019-10-24 02:54:11',
                'updated_at' => '2019-10-24 02:54:11',
            ),
            12 => 
            array (
                'id' => 13,
                'serialNo' => '1024_Limousine_13',
                'name' => 'Bentley Mulsanne',
                'description' => 'Essentially a less-expensive answer to the Rolls-Royce Phantom, the Bentley Mulsanne is a formal car that offers a number of options including electrically-operated privacy curtains in the passenger compartment that resemble vertical louvres.',
                'image_path' => 'images/1571885701.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2019-10-24 02:55:01',
                'updated_at' => '2019-10-24 02:55:01',
            ),
            13 => 
            array (
                'id' => 14,
                'serialNo' => '1024_Limousine_14',
                'name' => 'Cadillac One',
                'description' => 'There is a reason for only one flat price listing. The Cadillac One has only one customer—the President of the United States. Based on the DTS four-door sedan, the successor to the former, venerable de Ville line, the Cadillac One is not a limousine you can buy for a standard car service.',
                'image_path' => 'images/1571885732.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2019-10-24 02:55:32',
                'updated_at' => '2019-10-24 02:55:32',
            ),
            14 => 
            array (
                'id' => 15,
                'serialNo' => '1024_Limousine_15',
                'name' => 'Chrysler 300 Limousine',
                'description' => 'A very stretched version of Chrysler’s 300 sedan, the middle-section stretch is almost as long as the standard 300 sedan. The car features a V8 HEMI engine and is considered by numerous limousine services to be one of the best stretch limousines.',
                'image_path' => 'images/1571886982_5.jpg',
                'isActive' => 1,
                'category_id' => 5,
                'created_at' => '2019-10-24 02:56:45',
                'updated_at' => '2019-10-24 03:16:22',
            ),
            15 => 
            array (
                'id' => 16,
                'serialNo' => '1024_Yacht_16',
                'name' => 'History Supreme',
                'description' => '100-foot vessel was designed by world-renowned UK luxury designer Stuart Hughes, took over 3 years to complete and was purchased by an anonymous Malaysian businessman for a whopping $4.5 billion dollars!',
                'image_path' => 'images/1571886067.jpg',
                'isActive' => 1,
                'category_id' => 6,
                'created_at' => '2019-10-24 03:01:07',
                'updated_at' => '2019-10-24 03:01:07',
            ),
            16 => 
            array (
                'id' => 17,
                'serialNo' => '1024_Yacht_17',
                'name' => 'Eclipse',
                'description' => 'Russian billionaire Roman Abramovich is owner of the Eclipse.it was built by Blohm and Voss of Hamburg and measures 536 feet long also making it the second largest in the world.',
                'image_path' => 'images/1571886127.jpg',
                'isActive' => 1,
                'category_id' => 6,
                'created_at' => '2019-10-24 03:02:07',
                'updated_at' => '2019-10-24 03:02:07',
            ),
            17 => 
            array (
                'id' => 18,
                'serialNo' => '1024_Yacht_18',
                'name' => 'Azzam',
                'description' => 'Sheikh Khalifa bin Zayed al-Nayan, President of the United Arab Emirates and Emir of Abu Dhabi is the owner of this great $640 Million floating kingdom.Measuring in at 590 feet long, this floating extravagance boasts the largest private yacht in the world.',
                'image_path' => 'images/1571886331_6.jpg',
                'isActive' => 1,
                'category_id' => 6,
                'created_at' => '2019-10-24 03:02:52',
                'updated_at' => '2019-10-24 03:05:31',
            ),
            18 => 
            array (
                'id' => 19,
                'serialNo' => '1024_Yacht_19',
                'name' => 'Topaz',
                'description' => 'Topaz is 482 feet long with a beam length of 70.5 feet and a gross tonnage of 11,589.It comes with a power-packed 7990 HP engine to pull at a service speed of 22.9 knots, accelerating to a maximum speed is over 25.5 knots.',
                'image_path' => 'images/1571886381_6.jpg',
                'isActive' => 1,
                'category_id' => 6,
                'created_at' => '2019-10-24 03:03:49',
                'updated_at' => '2019-10-24 03:05:31',
            ),
            19 => 
            array (
                'id' => 20,
                'serialNo' => '1024_Yacht_20',
                'name' => 'Dubai',
                'description' => 'The most striking features of this beautiful boat comprise the abundance of sunbathing areas, several jacuzzis and the swimming pool boasting elaborate handmade tiling.',
                'image_path' => 'images/1571904048_6.jpg',
                'isActive' => 1,
                'category_id' => 6,
                'created_at' => '2019-10-24 03:04:38',
                'updated_at' => '2019-10-24 08:00:48',
            ),
            20 => 
            array (
                'id' => 21,
                'serialNo' => '1024_Planes_21',
                'name' => 'AIRBUS A380 CUSTOM',
                'description' => 'Prince alwaleed bin talaahs’s custom private jet was ranked and worth by Forbes for a prize of $500million dollars. That why we could ascertain the prize of this jet. The empty airbus a380 custom has a double decker which serves as the prince private lodge. you can bet this with me. prince alwaleed bin talaah’s private jet is filled with 7 large living room, a conference room and a dining room, a guest lodge, an office and a master’s suite for himself and a spiral staircase, all made just for himself.',
                'image_path' => 'images/1571904073_7.jpg',
                'isActive' => 1,
                'category_id' => 7,
                'created_at' => '2019-10-24 03:09:16',
                'updated_at' => '2019-10-24 08:01:13',
            ),
            21 => 
            array (
                'id' => 22,
                'serialNo' => '1024_Planes_22',
                'name' => 'AIRBUS 747–88A VIP',
                'description' => 'We couldn’t ascertain the prize of this cooperate private jet, due to its luxury interior. Russian wealthiest billionaire had this jets interior decorated too fit his style. you can see it wasn’t designed with gold, but this airbus could travel a 9000 miles and could carry 400 passenger as recommended by the company. Due to all its features, this airbus is far stronger and luxurious than Russian president Vladimir Putin’s private jet.',
                'image_path' => 'images/1571904086_7.jpg',
                'isActive' => 1,
                'category_id' => 7,
                'created_at' => '2019-10-24 03:10:09',
                'updated_at' => '2019-10-24 08:01:26',
            ),
            22 => 
            array (
                'id' => 23,
                'serialNo' => '1024_Planes_23',
                'name' => 'BOEING 737 BUSINESS JET',
                'description' => 'Boeing 737 business jet are owned into two class of people <sultan of Bruno and prince al-Waleed bin talaah. The smallest of princess al- Waleed bin talaah’s jet. The prince as the most expensive private jet in the world ranked one on our list. He uses this private Boeing jet when he fills like travelling light. He bought the jet for a 200 million dollars. And the two royalties had the interior redesigned into their taste looking like a royal palace of the sky. They had it covered in gold. They both placed a royal throne at the center of the living room to show their worth.',
                'image_path' => 'images/1571886649.jpg',
                'isActive' => 1,
                'category_id' => 7,
                'created_at' => '2019-10-24 03:10:49',
                'updated_at' => '2019-10-24 03:10:50',
            ),
            23 => 
            array (
                'id' => 24,
                'serialNo' => '1024_Planes_24',
                'name' => 'Boeing 747–8 VIP',
                'description' => 'A Hong Kong American businessman named Joseph liau bought this luxurious jet. liau turned it himself into a hotel in the sky in disguise. Joseph bought this Boeing jet for a 160 million dollars and he had the interior furnished with an extra 100 million dollars. He wanted it to be the best of its kind. The jet bought from a Chinese Boeing branch could carry a 299 passenger on board and has large windows with a horse power engine and could travel a 4000 miles at its expense. He now use it for business travel and meeting in the air.',
                'image_path' => 'images/1571904100_7.jpg',
                'isActive' => 1,
                'category_id' => 7,
                'created_at' => '2019-10-24 03:11:38',
                'updated_at' => '2019-10-24 08:01:40',
            ),
            24 => 
            array (
                'id' => 25,
                'serialNo' => '1024_Planes_25',
                'name' => 'BOEING 767 33A',
                'description' => 'As anyone ever had of roman abramovich. The owner of the most famous rocking English football club named “CHELSEA”. He bought this luxurious jet for a 100 million dollars and had the interior stripped off and replace with high class suite to his taste. The Boeing bought from air France airport could travel a 6000 miles with no stop over.',
                'image_path' => 'images/1571904113_7.jpg',
                'isActive' => 1,
                'category_id' => 7,
                'created_at' => '2019-10-24 03:12:44',
                'updated_at' => '2019-10-24 08:01:53',
            ),
        ));
        
        
    }
}