<?php

use Illuminate\Database\Seeder;

class StatusesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('statuses')->delete();
        
        \DB::table('statuses')->insert(array (
            0 => 
            array (
                'id' => 1,
                'status' => 'Pending Request',
                'isActive' => 1,
                'created_at' => '2019-10-16 00:00:00',
                'updated_at' => '2019-10-16 00:00:00',
            ),
            1 => 
            array (
                'id' => 2,
                'status' => 'Approved',
                'isActive' => 1,
                'created_at' => '2019-10-16 00:00:00',
                'updated_at' => '2019-10-16 00:00:00',
            ),
            2 => 
            array (
                'id' => 3,
                'status' => 'Declined',
                'isActive' => 1,
                'created_at' => '2019-10-16 00:00:00',
                'updated_at' => '2019-10-16 12:44:06',
            ),
            3 => 
            array (
                'id' => 4,
                'status' => 'Cancelled',
                'isActive' => 1,
                'created_at' => '2019-10-16 00:00:00',
                'updated_at' => '2019-10-16 00:00:00',
            ),
            4 => 
            array (
                'id' => 5,
                'status' => 'Returned',
                'isActive' => 1,
                'created_at' => '2019-10-16 00:00:00',
                'updated_at' => '2019-10-16 00:00:00',
            ),
        ));
        
        
    }
}