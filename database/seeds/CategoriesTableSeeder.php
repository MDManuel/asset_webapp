<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('categories')->delete();
        
        \DB::table('categories')->insert(array (
            0 => 
            array (
                'id' => 1,
                'category_name' => 'LuxCar',
                'isActive' => 1,
                'created_at' => '2019-10-16 10:26:21',
                'updated_at' => '2019-10-16 10:26:40',
            ),
            1 => 
            array (
                'id' => 4,
                'category_name' => 'Pickup',
                'isActive' => 1,
                'created_at' => '2019-10-17 18:10:49',
                'updated_at' => '2019-10-17 18:10:49',
            ),
            2 => 
            array (
                'id' => 5,
                'category_name' => 'Limousine',
                'isActive' => 1,
                'created_at' => '2019-10-17 18:13:21',
                'updated_at' => '2019-10-17 18:13:21',
            ),
            3 => 
            array (
                'id' => 6,
                'category_name' => 'Yacht',
                'isActive' => 1,
                'created_at' => '2019-10-24 01:01:40',
                'updated_at' => '2019-10-24 01:01:40',
            ),
            4 => 
            array (
                'id' => 7,
                'category_name' => 'Planes',
                'isActive' => 1,
                'created_at' => '2019-10-24 01:02:10',
                'updated_at' => '2019-10-24 01:02:10',
            ),
        ));
        
        
    }
}