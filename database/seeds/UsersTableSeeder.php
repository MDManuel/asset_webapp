<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{

    /**
     * Auto generated seed file
     *
     * @return void
     */
    public function run()
    {
        

        \DB::table('users')->delete();
        
        \DB::table('users')->insert(array (
            0 => 
            array (
                'id' => 1,
                'name' => 'Heroku',
                'email' => 'admin@admin.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$iS090hznzMSQVohGBCxqzO6LmT.LVs9jWM1jMCteCARyRjIm7FhGK',
                'isActive' => 1,
                'remember_token' => NULL,
                'created_at' => '2019-10-17 12:05:36',
                'updated_at' => '2019-10-17 12:05:36',
                'role_id' => 1,
            ),
            1 => 
            array (
                'id' => 2,
                'name' => 'Jumbotron',
                'email' => 'user@user.com',
                'email_verified_at' => NULL,
                'password' => '$2y$10$/mqF/rjyLpJJZlXZa1mg7OwTfoknebDh/pzNd75yY7mKfmy2GYfmm',
                'isActive' => 1,
                'remember_token' => NULL,
                'created_at' => '2019-10-17 12:05:54',
                'updated_at' => '2019-10-17 12:05:54',
                'role_id' => 2,
            ),
        ));
        
        
    }
}